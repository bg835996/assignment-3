# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 12:57:02 2019

@author: bg835996
"""

import numpy as np

# Functions for calculating gradients 

def gradient_2point(f,dx):
    '''The gradient of array f assuming points are a distance dx apart
    using two-point differences'''
    
    if dx <= 0:
        raise ValueError('dx should be positive (dx>=0).')  
    
    # Initialised the array for the gradient
    dfdx = np.zeros_like(f)
    
    # Two point differences at the end points
    dfdx[0]  = (f[1] - f[0])/dx
    dfdx[-1] = (f[-1] - f[-2])/dx
    
    # Centered differences for the mid-points
    for i in range(1,len(f)-1):
        dfdx[i] = (f[i+1] - f[i-1])/(2*dx)
    
    return dfdx


def gradient_4point(f,dx):
    '''The gradient of array f assuming points are a distance dx apart
    using four-point differences'''
    
    if dx <= 0:
        raise ValueError('dx should be positive (dx>=0).')  
    
    # Initialised the array for the gradient
    dfdx = np.zeros_like(f)
    
    # Uncentered four-point differences at the end points
    dfdx[0]  = (2*f[3] - 9*f[2] + 18*f[1] - 11*f[0])/(6*dx)
    dfdx[1]  = (2*f[4] - 9*f[3] + 18*f[2] - 11*f[1])/(6*dx)
    dfdx[-2] = (-2*f[-5] + 9*f[-4] - 18*f[-3] + 11*f[-2])/(6*dx)
    dfdx[-1] = (-2*f[-4] + 9*f[-3] - 18*f[-2] + 11*f[-1])/(6*dx)
    
    # Centered four-point differences for the mid-points
    for i in range(2,len(f)-2):
        dfdx[i] = (-f[i+2] + 8*f[i+1] - 8*f[i-1] + f[i-2])/(12*dx)
    
    return dfdx
        
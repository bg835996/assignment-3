MTMW12 assignment 3

Python3 code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 2-point or 4-point differences, 
compare with the analytic solution -> (graphs + tables)
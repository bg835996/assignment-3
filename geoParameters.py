# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 12:44:07 2019

@author: bg835996
"""

import numpy as np

# Input parameters for calculating the geostrophic wind
# Import this file wherever you need these parameters and functions

pa   = 1e5       # the mean pressure (Pa)
pb   = 200.0     # the magnitude of the pressure variations (Pa)
f    = 1e-4      # the Coriolis parameter
rho  = 1.0       # density
L    = 2.4e6     # length scale of the pressure variations
ymin = 0.0       # minimum space dimension 
ymax = 1e6       # maximum space dimension

def pressure(y):
    "The pressure at given locations, y"
    return pa + pb*np.cos(y*np.pi/L)

def uExact(y):
    "The analytic geostrophic wind at given locations, y"
    return pb * np.pi/(rho*f*L) * np.sin(y*np.pi/L)

def geoWind(dpdy):
    "The numerical geostrophic wind as a function of pressure gradient"
    return -dpdy/(rho*f)



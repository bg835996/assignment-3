# -*- coding: utf-8 -*-
"""
Created on Tue Oct 15 13:08:35 2019

@author: bg835996
"""

'''
# MTMW12 assignment 3. 
# Somrath Kanoksirirath (26835996)

Python3 code to numerically differentiate the pressure in order to calculate
the geostrophic wind relation using 2-point or 4-point differences, 
compare with the analytic solution -> (graphs + tables)
'''

import numpy as np
import matplotlib.pyplot as plt
import differentiate as dif
import geoParameters as gp

def geostrophicWind(N, ymin=gp.ymin, ymax=gp.ymax, method='two-point'):
    '''Calculate analytical and numericlal solutions of the geostrophic wind
    between ymin and ymax with the number of intervals N
    using method = 'two-point'  : two-point differences
          method = 'four-point' : four-point differences
    Return: uExact=Analytical sol., u_2point=Numerical sol., y=range'''

    # Check and compute parameters
    if N <= 0:
        raise ValueError('Input: N should be a positive integer (N>0).') 
    if N<1 and method == 'two-point' :
        raise Exception('N must >=1 for two-point scheme')
    if N<4 and method == 'four-point' :
        raise Exception('N must >= 4 for four-point scheme')
    N = int(N)            # the number of intervals
    if ymax < ymin:
        raise ValueError('Input: ymax should be greater than ymin.')
    dy = (ymax - ymin)/N  # the length of the spacing
    
    # The spatial dimension, y
    y = np.linspace(ymin, ymax, N+1) 

    # The geostrophic wind calculated using the analytic gradient
    uExact = gp.uExact(y)

    # The pressure at the y points
    p = gp.pressure(y)

    # The pressure gradient and wind using 2 or 4-point differences
    if method == 'two-point' :
        dpdy = dif.gradient_2point(p, dy)
    elif method == 'four-point' :
        dpdy = dif.gradient_4point(p, dy)
    else:
        raise Exception('Input: method = \'two-point\' or \'four-point\'.')
    u_2point = gp.geoWind(dpdy)

    return uExact, u_2point, y


def Graph(y, Ana, Numer, path=None):
    '''To produce a graph of the numerical (Numer) and analytical (Ana) 
    solutions and a graph of the erros --- and save to path'''
    
    # Check and get the number of intervals, N
    if (len(y) != len(Ana)) or (len(y) != len(Numer)) :
        raise Exception('1D arrays (y, Ana and Numer) should have \
                        the same dimension.') 
    N = len(y) -1
    
    # Set up plot to use large fonts
    font = {'size' : 14}
    plt.rc('font', **font)

    # Plot the approximate and exact wind at y points 
    plt.figure(0)
    plt.plot(y/1000, Ana, 'k-', label='Exact')
    plt.plot(y/1000, Numer, '*k--', label='Two-point diff', \
             ms=12, markeredgewidth=1.5, markerfacecolor='none')
    plt.legend(loc='best')
    plt.xlabel('y (km)')
    plt.ylabel('u (m/s)')
    plt.title('Analytical and Numerical solutions for N=%i' % N)
    plt.tight_layout()
    if path != None:
        pathName = path + '_Compare_' + str(N) + '.pdf'
        plt.savefig(pathName)
    
    # Plot the error of wind at y points 
    plt.figure(1)
    plt.plot(y/1000, abs(Ana-Numer), 'k.-.')
    plt.xlabel('y (km)')
    plt.ylabel('Difference (m/s)')
    plt.title('Errors of the numerical solution for N=%i.' % N)
    plt.tight_layout()  
    if path != None:
        pathName = path + '_Error_' + str(N) + '.pdf'
        plt.savefig(pathName)
      
    plt.show()
    
    
def scaleExperiment(N0, times,  ymin=gp.ymin, ymax=gp.ymax, \
                    method='two-point'):
    '''Show the errors by varying the number of intervals N = N0*(10**times)
    where N0 is the base number of interval between ymin and ymax, and also
             the number of points (equally spacing) in the range we interested
    using method = 'two-point'  : two-point differences
          method = 'four-point' : four-point differences
    Print: column = Interested points (equally spacing)
           row    = error from N0 to N0*(10**times)
    '''
    # Check parameters
    if N0 <= 0:
        raise ValueError('Input: N0 should be a positive integer.') 
    if times <= 0:
        raise ValueError('Input: times should be a positive integer.') 
    N0 = int(N0)
    times = int(times)

    # Declare an array to save errors
    errorArray = np.ones((times,N0+1))
    
    # Calculate the errors
    for i in range(0,times):
        scale = 10**i  # Interval between interested points and Scale factor
        # Get solutions using N = N0*scale
        ana, numer, y = geostrophicWind(N0*scale, ymin, ymax, method) 
        errorArray[i,:] = abs(ana[::scale] - numer[::scale]) # Save some errors
            
    print('\nErrors by varying the number of intervals at some points')
    print('N0 =', N0)
    print('times =', times)
    print('ymin =', ymin)
    print('ymax =', ymax)
    print('method =', method)
    print(errorArray)
    
    pathName = method + '_Experiment_N0' + str(N0) + '.csv'
    np.savetxt(pathName, errorArray, delimiter=",")
    
    
def myMain():
    "My main function"    
    
    # Graph for Question 1 Part 1
    # N = 10, y = 0-1000 km
    ana, numer, y = geostrophicWind(10)
    Graph(y, ana, numer, 'Normal_2P')
    
    # Graph and Table for Question 1 Part 2
    # N = 10, y = 0-4800 km
    ana, numer, y = geostrophicWind(10, ymax=4.8*gp.ymax)
    Graph(y, ana, numer, 'Long_2P')
    # N0 = 5, y = 0-1000 km 
    scaleExperiment(5, 4)

    # ---------- ---------- ---------- 

    # Graph for Question 2 Part 1
    # N = 10, y = 0-1000 km
    ana, numer, y = geostrophicWind(10, method='four-point')
    Graph(y, ana, numer, 'Normal_4P')
    
    # Graph and Table for Question 1 Part 2
    # N = 10, y = 0-4800 km
    ana, numer, y = geostrophicWind(10, ymax=4.8*gp.ymax, method='four-point')
    Graph(y, ana, numer, 'Long_4P')
    # N0 = 5, y = 0-1000 km 
    scaleExperiment(5, 4, method='four-point')    
    
    
if __name__ == "__main__" :
    myMain()
    
